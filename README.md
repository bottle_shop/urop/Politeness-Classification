
# Politeness Classification

Introduce the task

## Data Collection and Annotation
Describe the data collection and annotation process.

## Exploratory Data Analysis
Describe the dataset statistics

## Politeness Classification Task

#### Baselines Explored
List the baselines explored (e.g., Decision Tree).

#### Experiment Results

|Model| Precision | Recall| F1| Accuracy |
|---|---|---|---|---|
| Logistic Regression | | | | |
| Decision Tree | | | | | |
| Text-CNN | | | | | |
| LSTM | | | | | |

